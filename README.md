## Desafio GitLab - Terraform - Bootcamp Avanti DevOps

Este projeto demonstra a configuração de uma infraestrutura na AWS usando Terraform para hospedar uma aplicação, a implementação de um deploy na EC2 com o GitLab, bem como a criação de um pipeline de CI/CD, garantindo atualizações automáticas desta aplicação sempre que ocorrerem mudanças no repositório.

### Provisionando uma infraestrutura com Terraform na AWS

1. Criar os arquivos maint.tf, ec2.tf, security-group.tf e script.sh.
Os código podem ser encontrados aqui: https://gitlab.com/luannylucena/novo/-/tree/main/terraform?ref_type=heads

2. Inicializar o projeto utilizando o comando **terraform init**

![init](images/init.png)

3. Executar o comando **terraform plan**. Ele permite revisar e validar as alterações planejadas antes de aplicá-las, evitando surpresas indesejadas.

![plan](images/plan.png)

4. Executar o comando **terraform apply**. O comando terraform apply executa as mudanças planejadas na infraestrutura conforme definido no código Terraform, criando, atualizando ou removendo recursos conforme necessário, e fornece feedback sobre o progresso das operações, além de um resumo das alterações feitas após a conclusão.

![apply](images/apply.png)

### Infraestutura criada:

![instancia](images/instancia.png)

### Criando um deploy com Gitlab na EC2

1. Vincular manualmente o servidor que foi criado ao projeto existente no GitLab que contém a aplicação que se deseja subir, seguindo os seguintes passos:
- Seetings
- CI/CD
- Runners
- Show runner installation and registration instructions
- Command to register runner
- Executa o comando exibido na EC2: **sudo gitlab-runner register --url https://gitlab.com/ --registration-token XXXXXXXXX-**

- Registra o Runner

![registro_runner](images/registro_runner.png)

- Runner criado:

![runners](images/runners.png)

2. Criar o arquivo Pipeline **.gitlab-ci.yml** na raiz do repositório e dar o push

- Pipeline criado:

![pipeline](images/pipeline.png)

- Aplicação rodando

![app](images/app.png)

Por fim, executar o comando **terraform destroy**. Sua função é desfazer todas as alterações feitas pela aplicação de terraform apply, revertendo a infraestrutura para o estado anterior à sua implementação. Isso significa que todos os recursos provisionados, como instâncias EC2, bancos de dados, grupos de segurança, entre outros, serão desativados e removidos.

![destroy](images/destroy.png)


















